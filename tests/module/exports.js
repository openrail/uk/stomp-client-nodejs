const { expect } = require('chai');

const Module = require('../../index.js');

const stomp = require('../../lib/common/stomp');

module.exports = function () {
  describe('Export suite', function () {
    it('Should export the module', function() {
      expect(Module).to.be.an('object');
    });

    it('Should export StompClient class', function() {
      expect(Module.StompClient).to.be.an('function');
      expect(new Module.StompClient()).to.be.an('object');
      expect(Module.StompClient).to.be.equal(stomp.StompClient);
    });
  });
};