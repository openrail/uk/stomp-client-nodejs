const moduleSuite = require('./module/module');

const stompSuite = require('./stomp/stomp');

describe('@openrailuk/stomp-client test suite', function () {
  moduleSuite();
  stompSuite();
});
