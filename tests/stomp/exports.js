'use strict';

const { expect } = require('chai');

const model = require('../../lib/common/stomp');

const { StompClient } = require('../../lib/common/stomp');


module.exports = function () {
  describe('Export suite', function () {
    it('Should export', function() {
      expect(model).to.be.an('object');

      expect(model).to.have.all.keys([
        'StompClient',
        'symbols'
      ]);
    });

    it('Should export StompClient Class', function () {
      expect(model.StompClient).to.exist;
      expect(model.StompClient).to.be.an('function');
    });
  });
};
