'use strict';

const { expect } = require('chai');

const Server = require('stompit/lib/Server');
const Socket = require('stompit/lib/Socket');

let model = require('../../lib/common/stomp');

let server = null;

const host = '127.0.0.1';
const port = 8080;

module.exports = function () {
  describe('Functional suite', function () {
    this.beforeAll(function() {
      // server = new Server();
    });

    describe('initialise() tests', function () {
      it('should construct a new instance of Stomp Client', function () {
        const unit = new model.StompClient({ host, port });

        expect(unit).to.be.an.instanceOf(model.StompClient);
      });
    });
  });
};
