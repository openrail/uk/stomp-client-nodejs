import zlib from 'zlib';
import stompit from 'stompit';
import EventEmitter from 'events';

export const symbols = new Map()
  .set('host', Symbol('host'))
  .set('port', Symbol('port'))
  .set('timeout', Symbol('timeout'))
  .set('channel', Symbol('channel'))
  .set('subscriptions', Symbol('subscriptions'));

export class StompClient extends EventEmitter {
  constructor(options = {}) {
    super();

    this[symbols.get('host')] = options.host;
    this[symbols.get('port')] = options.port;
    this[symbols.get('timeout')] = options.timeout;

    this[symbols.get('channel')] = null;

    this[symbols.get('subscriptions')] = [];
  }

  /** 
   * @member {String} host gets the host address for the stomp server
   * @memberof StompClient
   * @readonly
   * @instance
   * @public
   */
  get host() {
    return this[symbols.get('host')] || 'datafeeds.nationalrail.co.uk';
  }

  /** 
   * @member {Number} port gets the port number for the stomp server
   * @memberof StompClient
   * @readonly
   * @instance
   * @public
   */
  get port() {
    return this[symbols.get('port')] || 61613;
  }

  /** 
   * @member {Number} timeout gets the timeout in ms for the client
   * @memberof StompClient
   * @readonly
   * @instance
   * @public
   */
  get timeout() {
    return this[symbols.get('timeout')] || 3000;
  }

  /** 
   * @member {Object} timeout gets the servers configuration
   * @memberof StompClient
   * @readonly
   * @instance
   * @public
   */
  get serverConfig() {
    return {
      host: this.host,
      port: this.port,
      timeout: this.timeout,
      connectHeaders: {
        host: '/',
        login: 'd3user',
        passcode: 'd3password',
        'heart-beat': '5000,5000'
      }
    }
  }

  /** 
   * @member {Object} isInitialised determines if client is initialised
   * @memberof StompClient
   * @readonly
   * @instance
   * @public
   */
  get isInitialised() {
    return (this[symbols.get('channel')] !== null) || false;
  }

  /**
   * @method initialise
   * @memberof StompClient
   * @description initialises the stomp client
   * @author Steven Collins <steven@carboncollins.uk>
   * @instance
   */
  initialise() {
    return new Promise((resolve, reject) => {
      if (!this.isInitialised) {
        const servers = new stompit.ConnectFailover([this.serverConfig]);
        
        servers.on('error', (...args) => {
          this.emit('error', ...args);
        });
        
        servers.on('connecting', (...args) => {
          this.emit('connecting', ...args);
        });
        
        servers.on('connect', (...args) => {
          this.emit('connected', ...args);
        });
        
        this[symbols.get('channel')] = new stompit.Channel(servers, { alwaysConnected: true });
        
        this[symbols.get('channel')].on('error', (...args) => {
          this.emit('error', ...args);
        });
        
        this[symbols.get('channel')].on('idle', () => {
          this.emit('idle');
        });

        this[symbols.get('subscriptions')] = [];

        resolve();
      } else {
        reject(new Error('Client has already been initialised'));
      }
    });
  }

  /**
   * @method subscribe
   * @memberof StompClient
   * @description subscribes to a queue on the stomp server
   * @author Steven Collins <steven@carboncollins.uk>
   * @instance
   */
  subscribe(queue) {
    return new Promise((resolve, reject) => {
      if (this.isInitialised) {
        this[symbols.get('channel')].subscribe(queue, (err, message) => {
          const type = message.headers.FilterHeaderLevel;
          let buffer = Buffer([]);
      
          message
            .pipe(zlib.createGunzip())
            .on('error', (err) => {
              this.emit('error', err);
            })
            .on('data', (chunk) => {
              buffer = Buffer.concat([buffer, chunk]);
            })
            .on('end', () => {
              const xmlMessage = buffer.toString('utf-8');
              this.emit('message', {
                message: xmlMessage,
                headers: message.headers,
                queue,
                type
              });

              switch (type) {
                case 'TRAINSTATUS,':
                  this.emit('message#trainStatus', {
                    queue,
                    message: xmlMessage
                  });
                  break;
                case 'SCHEDULE,':
                  this.emit('message#schedule', {
                    queue,
                    message: xmlMessage
                  });
                  break;
                case 'ASSOCIATION,':
                  this.emit('message#association', {
                    queue,
                    message: xmlMessage
                  });
                  break;
                case 'TRAINORDER,':
                  this.emit('message#trainOrder', {
                    queue,
                    message: xmlMessage
                  });
                  break;
                case 'STATIONMESSAGE,':
                  this.emit('message#stationMessage', {
                    queue,
                    message: xmlMessage
                  });
                  break;
                default:
                  break;
              }
            });
        });

        this[symbols.get('subscriptions')].push(queue);

        resolve();
      } else {
        reject('STOMP client needs to be initiliased before you can subscribe to a queue');
      }
    });
  }

  /**
   * @method close
   * @memberof StompClient
   * @description closes any connections to the STOMP server and unsubscibes from any queues
   * @author Steven Collins <steven@carboncollins.uk>
   * @instance
   */
  close() {
    return new Promise((resolve, reject) => {
      if (this.isInitialised) {
        this[symbols.get('channel')].close();
        this[symbols.get('subscriptions')] = [];

        resolve();
      } else {
        reject(new Error('client needs to be initialised in order to be able to close it'));
      }
    });
  }
}
